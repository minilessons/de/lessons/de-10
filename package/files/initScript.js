let canvas = document.getElementById("testCanvas");
let myCanvas;

let opts = {
    id: "testCanvas",
    readOnly: false
};

if (canvas !== null) {
    let constants = getLessonConstants();
    myCanvas = new CanvasX(canvas, opts, constants);

    createUI(myCanvas);

    // Create main program
    let main = new MainProgram(myCanvas);

} else {
    throw ("Canvas not defined!");
}


/*
let restrictRect = new Rectangle("restrictRect", 0, 100, myCanvas.width, myCanvas.height, convertHexToRGBA("#FFFFFF", 0.0), {});
let drawComp = new DrawComponent(restrictRect);*/


function lineMouseMove(e, shape, mouseX, mouseY, button, inside, canvas) {
    shape.selected = inside;
}


function checkAcceptance(canvasX) {

    function getDistance(state, other) {
        let c = state.circle;
        return Math.sqrt((Math.pow(c.x - other.circle.x, 2) + Math.pow(c.y - other.circle.y, 2)));
    }


    let activeState = canvasX.getShapeByID(canvasX.activeStateID);
    let isSpotAvailable = true;
    canvasX.shapes.forEach((sh) => {
        let trimmed = sh.ID.trim();
        if (trimmed.startsWith("State") && trimmed !== activeState.ID) {
            let distanceFromCenters = getDistance(sh, activeState);
            if (distanceFromCenters < 2 * sh.circle.radius) {
                isSpotAvailable = false;
            }
        }
    });

    return isSpotAvailable;
}


function globalMouseUp(e, canvasX) {
    if (true === canvasX.dragg) {
        // Dragging was active
        // Check acceptance, or return to saved state
        if (true === checkAcceptance(canvasX)) {
            // Do nothing
            canvasX.dragg = false;
        } else {
            canvasX.removeShapeByID(activeStateID);
            canvasX.addShape(canvasX.tempActiveState);
            canvasX.dragg = false;
        }
    }
}


function restoreToTempState(sh, initialState) {
    sh.circle.x = initialState.cx;
    sh.circle.y = initialState.cy;
    sh.textArea.x = initialState.areaX;
    sh.textArea.y = initialState.areaY;
}


function globalMouseDown(e, canvasX) {
    if (activeAddState === true) {
        return;
    }

    let mouse = getMouse(canvasX.canvas, e);
    let type = getButtonType(e);
    let selectedStateID = canvasX.activeStateID;


    if (true === canvasX.dragg) {
        let sh = canvasX.getShapeByID(selectedStateID);
        switch (type) {
            case "right":
                console.log("Detaching, user don't want to accept move change");
                restoreToTempState(sh, canvasX.tempActiveState);
                canvasX.selectedShapes.pop();
                break;
            case "left":
                // Change accepted?
                if (true === checkAcceptance(canvasX)) {
                    console.log("Change accepted with click", type);
                    canvasX.selectedShapes.pop();
                } else {
                    console.log("Not accepted for id:", selectedStateID);
                    restoreToTempState(sh, canvasX.tempActiveState);
                    canvasX.selectedShapes.pop();
                }

                break;
        }
        sh.setSelection(false);
        canvasX.dragg = false;
        return;
    }


    let size = canvasX.selectedShapes.length;
    if (size === 0) {
        return;
    }

    console.log("Got to selections", canvasX.selectedShapes);
    let lastActiveID = canvasX.selectedShapes[size - 1];
    if (!lastActiveID.startsWith("State")) {
        return;
    }

    console.log("lastID: ", lastActiveID);
    let lastActive = canvasX.getShapeByID(lastActiveID);

    canvasX.tempActiveState = {
        cx: lastActive.circle.x,
        cy: lastActive.circle.y,
        areaX: lastActive.textArea.x,
        areaY: lastActive.textArea.y,
    };
    canvasX.activeStateID = lastActive.ID;
    canvasX.dragg = true;
    canvasX.draggX = mouse.x;
    canvasX.draggY = mouse.y;

}


function globalMouseMove(e, canvasX) {
    if (activeAddState === true) {
        return;
    }
    let mouse = getMouse(canvasX.canvas, e);

    if (true === canvasX.dragg) {
        // Set active state values
        let activeState = canvasX.getShapeByID(canvasX.activeStateID);
        activeStateMoveEvent(e, activeState, mouse.x, mouse.y, undefined, undefined, canvasX);

        canvasX.draggX = mouse.x;
        canvasX.draggY = mouse.y;
    }
}


let activeAddState = false;
let activeStateID = "State " + 0;


function btnMouseDown(e, shape, mouseX, mouseY, button, inside, canvas) {
    if (!inside) {
        try {
            if (!activeAddState) {
                return false;
            }

            let activeState = canvas.getShapeByID(activeStateID);
            if (button === "left" && activeState.isVisible) {
                // State is already in canvas, set its selection to false, and detach event from it;
                activeState.setSelection(false);
                canvas.removeRegisteredEvent(activeStateID);

                // Adjust options and increment ID
                let elems = activeStateID.split(" ");
                activeStateID = elems[0] + " " + (parseInt(elems[1]) + 1);
                activeAddState = false;

                canvas.selectedShapes.pop();
                console.log("Added state with ID:" + activeState.ID);
            } else if (button === "right") {
                // Remove active state and detach event for it, user don't want to add this state
                canvas.removeShapeByID(activeStateID);
                canvas.removeRegisteredEvent(activeStateID);
                console.log("detaching events!");
                activeAddState = false;
            } else {
                // Cannot add the state, leave it active until next shot
                // Do nothing
            }
            return true;
        } catch (e) {
            console.log("No registered events found!");
            console.log(e);
            return;
        }
    } else {
        console.log("Sh", shape.selected);
        if (shape.selected) {
            shape.selected = false;
        }
    }

    if (activeAddState === true) {
        // Do nothing if user is already adding a state
        return true;
    }

    // Set flag to active
    activeAddState = true;

    // Inside button, create new automate state and attach event to it;
    const radius = Math.PI * 12;
    let idx = 0;
    let activeCircle = new Circle(activeStateID, mouseX, mouseY, radius, {fillStyle: canvas.constants.colors.dodgerBlue});


    let textOpts = {
        font: canvas.constants.fonts.normalFont,
        textHeight: 16,

    };
    let textArea = new TextField(activeStateID, activeStateID, mouseX - radius, mouseY - radius, 2 * radius, radius, textOpts);
    let activeState = new AutomateState(activeStateID, activeCircle, textArea);

    activeState.isVisible = false;
    canvas.addShape(activeState);

    // Add event listener, which will activate adding state on mouse move
    let automateStateEvent = {
        ID: activeStateID,
        type: "mousemove",
        shapeID: activeStateID,
        alwaysCall: true,
        callback: activeStateMoveEvent
    };

    canvas.registerEvent(automateStateEvent);

    return true;

}


function activeStateMoveEvent(e, shape, mouseX, mouseY, button, inside, canvas) {
    let c = shape.circle;
    let safetyOffset = 10;
    shape.isVisible = drawComp.contains(mouseX - c.radius - safetyOffset, mouseY - c.radius - safetyOffset);
    c.x = mouseX;
    c.y = mouseY;
    shape.textArea.x = mouseX - c.radius / 1.5;
    shape.textArea.y = mouseY - c.radius / 1.5;


    shape.updateTransitions(canvas);
}


function createUI(canvas) {
    // Create line and add it to canvas
    let height = 100;
    let separatorID = "uiSeparator";
    let uiSeparator = new Line(separatorID, 0, height, canvas.width, height, {strokeStyle: canvas.constants.colors.black});
    canvas.addShape(uiSeparator);

    // Create event and register it to canvas
    let lineEvent = {
        ID: "lineEvent",
        type: "mousemove",
        alwaysCall: true,
        callback: lineMouseMove,
        shapeID: separatorID
    };

    //canvas.registerEvent(lineEvent);

    // Create buttons
    let btnHeight = 25;
    let btnWidth = 90;
    let btnStartY = 10;
    let btnStartX = 200;

    // Create add state button
    let addBtnOpts = {
        curved: true,
        leftCurve: 4,
        rightCurve: 4,
        font: canvas.constants.fonts.normalFont
    };
    let addID = canvas.constants.strings.addID;
    let addStateBtn = new MyButton(addID, btnStartX + 100, btnStartY, btnWidth, btnHeight, "Add State", addBtnOpts);

    canvas.addShape(addStateBtn);

    // Register events for add state button
    let addStateEvent = {
        ID: addID,
        type: "mousedown",
        alwaysCall: true,
        callback: btnMouseDown,
        shapeID: addID
    };
    //canvas.registerEvent(addStateEvent);


    // Create export button
    let exportBtnOpts = {
        curved: true,
        leftCurve: 4,
        rightCurve: 4,
        font: canvas.constants.fonts.normalFont,
        textMaxSpan: btnWidth
    };

    let exportBtnId = canvas.constants.strings.exportBtnID;
    let exportStateBtn = new MyButton(exportBtnId, btnStartX + 200, btnStartY, btnWidth, btnHeight, "Export State", exportBtnOpts);

    canvas.addShape(exportStateBtn);

    // Register events for export state button
    let exportBtnEvent = {
        ID: exportBtnId,
        type: "mousedown",
        alwaysCall: true,
        callback: btnMouseDownDispatch,
        shapeID: exportBtnId
    };

    //canvas.registerEvent(exportBtnEvent);

    let importBtnId = canvas.constants.strings.importBtnID;
    let importBtn = new MyButton(importBtnId, btnStartX + 300, btnStartY, btnWidth, btnHeight, "Import State", exportBtnOpts);

    canvas.addShape(importBtn);

    // Register events for add state button
    let importBtnEvent = {
        ID: importBtnId,
        type: "mousedown",
        alwaysCall: true,
        callback: btnMouseDownDispatch,
        shapeID: importBtnId
    };

    //canvas.registerEvent(importBtnEvent);


    let addLinkId = canvas.constants.strings.addTransitionBtnID;
    let addLinkBtn = new MyButton(addLinkId, btnStartX + 400, btnStartY, btnWidth, btnHeight, "Add Link", exportBtnOpts);
    canvas.addShape(addLinkBtn);

    let addLinkBtnEvent = {
        ID: addLinkId,
        type: "mousedown",
        alwaysCall: true,
        callback: btnMouseDownDispatch,
        shapeID: addLinkId
    };


    //canvas.registerEvent(addLinkBtnEvent);

    let removeStatesId = canvas.constants.strings.removeStateBtnID;
    let removeStatesBtn = new MyButton(removeStatesId, btnStartX - 25, btnStartY, btnWidth + 25, btnHeight, "Remove selected", exportBtnOpts);

    canvas.addShape(removeStatesBtn);

    let removeStatesEvent = {
        ID: removeStatesId,
        type: "mousedown",
        alwaysCall: true,
        callback: btnMouseDownDispatch,
        shapeID: removeStatesId
    };


    //canvas.registerEvent(removeStatesEvent);
    canvas.invalidate();

}


function exportStateToArea(canvas) {
    let area = document.getElementById("outputLogger");

    //area.innerHTML = canvas.exportState();
    area.innerHTML = canvas.exportWithCondition((elem) => {
        return elem.ID.toLowerCase().startsWith("state") || elem.ID.toLowerCase().startsWith("transition");
    });

}


function constructTransitionId(state1Id, state2Id) {
    return "transition" + state1Id + "_" + state2Id;
}


function importStateFromArea(canvas) {
    let area = document.getElementById("outputLogger");
    let parsedJSON = JSON.parse(area.innerHTML);

    canvas.resetCanvas();
    createUI(canvas);
    parsedJSON.forEach((x) => {
        let shape;
        if (x.ID.toLowerCase().startsWith("transition")) {
            let newVertices = x.curve.vertices;
            let newCurve = new BezierCurve(newVertices, x.curve.divs);
            let transitionId = constructTransitionId(x.state1Id, x.state2Id);
            shape = new Transition(transitionId, newCurve, x.state1Id, x.state2Id);

            let state1 = canvas.getShapeByID(x.state1Id);
            let state2 = canvas.getShapeByID(x.state2Id);
            state1.addTransition(transitionId);
            state2.addTransition(transitionId);
        } else {
            let c = x.circle;
            let ar = x.textArea;
            let circle = new Circle(c.ID, c.x, c.y, c.radius, c.options);
            let textArea = new TextField(ar.ID, ar.text, ar.x, ar.y, ar.w, ar.h, ar.options);
            shape = new AutomateState(x.ID, circle, textArea);
        }

        canvas.addShape(shape);
    });
}


function btnMouseDownDispatch(e, shape, mouseX, mouseY, button, inside, canvas) {
    console.log("Dispatch called");
    if (!inside) {
        return;
    }

    // Which button is pressed
    switch (shape.ID) {
        case canvas.constants.strings.exportBtnID:
            // Export to text area
            exportStateToArea(canvas);
            break;
        case canvas.constants.strings.importBtnID:
            importStateFromArea(canvas);
            break;
        case canvas.constants.strings.addTransitionBtnID:
            console.log("called add link");
            connectStates(canvas);
            break;
        case canvas.constants.strings.removeStateBtnID:
            removeSelectedStates(canvas);
            break;
    }
}


function removeSelectedStates(canvas) {
    let selections = canvas.selectedShapes;
    selections.forEach((shapeId) => {
        let sh = canvas.getShapeByID(shapeId);
        sh.transitions.forEach((transitionId) => {
            let transition = canvas.getShapeByID(transitionId);
            transition.removeTransition(canvas);
            canvas.removeShapeByID(transitionId);
        });

        canvas.removeElementByID(canvas.selectedShapes, sh.ID);
        canvas.removeShapeByID(shapeId);
    });


}


function connectStates(can) {
    if (can.selectedShapes.length !== 2) {
        alert("Please select exactly 2 states for adding link. You selected: " + can.selectedShapes.length + " states.");
        return;
    }

    let state1Id = can.selectedShapes[0];
    let state2Id = can.selectedShapes[1];
    let state1 = can.getShapeByID(state1Id);
    let state2 = can.getShapeByID(state2Id);

    if (!state1 || !state2) {
        alert("Not valid states");
    } else {
        console.log("Creating bezier");

        let vertices = constructVertices(state1, state2);
        let curve = new BezierCurve(vertices, 50);

        let transitionId = constructTransitionId(state1Id, state2Id);
        let transition = new Transition(transitionId, curve, state1Id, state2Id);
        state1.addTransition(transitionId);
        state2.addTransition(transitionId);

        can.addShape(transition);
    }

    state1.setSelection(false);
    state2.setSelection(false);
    can.selectedShapes = [];
    //can.removeElementByID(can.selectedShapes, state1Id);
    //can.removeElementByID(can.selectedShapes, state2Id);
    can.invalidate();
}

