/**
 * Function constructor which initializes a Rectangle.
 *
 * @param ID object identification, for complex objects same ID is given
 * @param x top left x coordinate of a Rectangle
 * @param y top left y coordinate of a Rectangle
 * @param w width of the Rectangle
 * @param h height of the Rectangle
 * @param fillStyle Rectangle fill
 * @param opts other options
 * @constructor
 */
function Rectangle(ID, x, y, w, h, fillStyle, opts) {
    this.ID = ID || undefined;

    // Rectangle dimensions
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;

    // Rectangle style

    // Stroke and fill options
    this.fillShape = (opts.fillShape === undefined) ? true : opts.fillShape;
    this.strokeShape = (opts.strokeShape === undefined) ? true : opts.strokeShape;

    // Context style
    this.fillStyle = fillStyle || '#AAAAAA';

    // Selection options
    this.selected = false;
    this.hovered = false;
    this.selectionColor = opts.selectionColor || "#4123A4";


    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for Line shape.
 *
 * @param ID identification
 * @param x0 x coordinate of start point
 * @param y0 y coordinate of start point
 * @param x1 x coordinate of end point
 * @param y1 y coordinate of end point
 * @param opts other options
 * @param groupID group identification
 * @constructor
 */
function Line(ID, x0, y0, x1, y1, opts, groupID) {
    this.ID = ID || undefined;
    this.groupID = groupID || undefined;

    this.x0 = x0 || 0;
    this.x1 = x1 || 0;
    this.y0 = y0 || 1;
    this.y1 = y1 || 1;

    // Stroke and fill options
    this.fillShape = (opts.fillShape === undefined) ? true : opts.fillShape;
    this.strokeShape = (opts.strokeShape === undefined) ? true : opts.strokeShape;


    // Selection options:
    this.selected = false;
    this.hovered = false;

    // Line style
    this.strokeStyle = opts.strokeStyle || "#000000";

    this.lineWidth = opts.lineWidth || 1;
    this.lineDash = opts.lineDash || [];

    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for text field.
 *
 * @param ID object identification string
 * @param text text which is shown in text field
 * @param x top left x coordinate for text field
 * @param y top left y coordinate for text field
 * @param w width of the field
 * @param h height of the field
 * @param opts other options
 * @constructor
 */
function TextField(ID, text, x, y, w, h, opts) {
    this.ID = ID || undefined;

    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;

    // Text string to show
    this.text = text || "";

    // Text style

    // Stroke and fill options
    this.fillShape = (opts.fillShape === undefined) ? true : opts.fillShape;
    this.strokeShape = (opts.strokeShape === undefined) ? true : opts.strokeShape;

    // Context style
    this.fillStyle = opts.fillStyle || "#000000";
    this.strokeStyle = opts.strokeStyle || "#000000";

    this.font = opts.font || "12px Courier";
    this.textHeight = opts.textHeight || 12;
    this.textAlign = opts.textAlign || "center";
    this.textBaseline = opts.baseline || "middle";
    this.textMaxSpan = opts.textMaxSpan || 150;

    // Save options for later usage
    this.options = opts;

    // Selection options
    this.selected = false;
    this.hovered = false;

    // Visibility
    this.isVisible = true;
}


/**
 * Function constructor for simple Circle shape.
 *
 * @param ID object identification string
 * @param centerX x value for circle center
 * @param centerY y value for circle center
 * @param radius radius of a circle
 * @param opts other options
 * @constructor
 */
function Circle(ID, centerX, centerY, radius, opts) {
    this.ID = ID || undefined;
    this.x = centerX || 50;
    this.y = centerY || 50;
    this.radius = radius || Math.PI;

    this.startAngle = opts.startAngle || 0;
    this.endAngle = opts.endAngle || 360;
    this.rotation = opts.rotation || false;

    // Circle style
    this.alphaNonTransparent = opts.alphaNonTransparent || 1;
    this.alphaTransparent = opts.alphaTransparent || 0.5;


    // Stroke and fill options
    this.fillShape = (opts.fillShape === undefined) ? true : opts.fillShape;
    this.strokeShape = (opts.strokeShape === undefined) ? true : opts.strokeShape;

    // Context style
    this.fillStyle = opts.fillStyle || "#FFFFFF";
    this.strokeStyle = opts.strokeStyle || "#000000";

    this.lineWidth = opts.lineWidth || 1;

    // Selection options
    this.selected = false;
    this.hovered = false;
    this.selectionColor = opts.selectionColor || "#4123A4";

    // Save all options
    this.options = opts;

    // Visibility
    this.isVisible = true;
}


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Draw prototypes for all shapes
   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/**
 * Actual drawing of shape Circle.
 *
 * @param ctx context to draw to
 */
Rectangle.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.rect(this.x, this.y, this.w, this.h);
    canvasStrokeAndFill(this, ctx);
    ctx.restore();
};

/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Rectangle.prototype.draw = function (ctx) {
    setupContext(ctx, this);
    if (true === this.selected) {
        ctx.save();
        setDefaultSelection(ctx, this.shadowBlur, this.selectionColor);
        this.drawShape(ctx);
        ctx.restore();
    } else {
        this.drawShape(ctx);
    }
};

/**
 * Actual drawing of shape Circle.
 *
 * @param ctx context to draw to
 */
Circle.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, this.startAngle, this.endAngle, this.rotation);
    canvasStrokeAndFill(this, ctx);
    ctx.restore();
};

/**
 * Prototype which is used to draw this shape object.
 *
 * @param ctx context to draw to
 */
Circle.prototype.draw = function (ctx) {
    setupContext(ctx, this);
    if (true === this.hovered) {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaNonTransparent).cssString();
        this.drawShape(ctx);
        ctx.restore();
    } else {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaTransparent).cssString();
        setNoSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    }

    if (true === this.selected) {
        ctx.save();
        // Draw additional circle around to show selection
        let tempCircle = new Circle("tempC", this.x, this.y, this.radius + 10, this.options);
        setupContext(ctx, tempCircle);
        ctx.beginPath();
        ctx.arc(tempCircle.x, tempCircle.y, tempCircle.radius, tempCircle.startAngle, tempCircle.endAngle, tempCircle.rotation);
        ctx.arc(tempCircle.x, tempCircle.y, tempCircle.radius - 0.5, tempCircle.startAngle, tempCircle.endAngle, tempCircle.rotation);
        ctx.fill('evenodd');
        ctx.mozFillRule = 'evenodd';
        ctx.restore();
    }
};

/**
 * Actual drawing of Line objet.
 *
 * @param ctx context to draw to
 */
Line.prototype.drawShape = function (ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(this.x0, this.y0);
    ctx.lineTo(this.x1, this.y1);
    canvasStrokeAndFill(this, ctx);
    ctx.restore();
};


/**
 * Prototype which is used to draw Line object.
 *
 * @param ctx context to draw to
 */
Line.prototype.draw = function (ctx) {
    setupContext(ctx, this);
    if (true === this.selected) {
        ctx.save();
        setPointOfSelection(ctx, this);
        this.drawShape(ctx);
        ctx.restore();
    } else {
        this.drawShape(ctx);
    }
};


/**
 * Actual drawing of shape TextField.
 *
 * @param ctx context to draw to
 */
TextField.prototype.drawShape = function (ctx) {
    // MultiLineText
    let lines = this.text.split("\n");
    let numOfLines = lines.length;
    let offsetBetweenLines = 3;
    let lineHeight = this.textHeight + offsetBetweenLines;
    ctx.textMaxSpan = 80;

    let centerY = (this.h - numOfLines * this.textHeight) / 2;
    for (let c = 0; c < numOfLines; c++) {
        let textWidth = ctx.measureText(lines[c]).width;
        let numOfLetters = lines[c].length;
        let oneLetterUnit = textWidth / numOfLetters;

        let seenLetters = numOfLetters;
        let minOffset = 3;
        let widthOffset = 5;
        for (let c = 1; c <= numOfLetters; c++) {
            if (this.w < (minOffset + widthOffset + oneLetterUnit * c)) {
                seenLetters = c - 1;
                break;
            }
        }

        let centerX = (this.w - textWidth) / 2;

        let xText = Math.max(this.x + minOffset, minOffset + this.x + centerX);

        let textString = lines[c].slice(0, seenLetters);
        ctx.fillText(textString, xText, this.y + centerY + lineHeight * c + this.textHeight / 2, this.textMaxSpan);
    }

    ctx.stroke();

};


/**
 * Prototype which is used to draw TextField object.
 *
 * @param ctx context to draw to
 */
TextField.prototype.draw = function (ctx) {
    ctx.save();
    setupContext(ctx, this);
    this.drawShape(ctx);
    ctx.restore();
};


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Cointains functions for all shapes
   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


/**
 * Checks whether Shape object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Rectangle.prototype.contains = function (mx, my) {
    return containsForRectangle(mx, my, this);
};

/**
 * Checks whether mouse is contained inside circle.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {boolean} true if contains
 */
Circle.prototype.contains = function (mx, my) {
    let r = this.radius;
    let dx = mx - this.x;
    let dy = my - this.y;
    return ((dx * dx) + (dy * dy)) < (r * r);
};


/**
 * Checks whether Line object contains point where mouse is at.
 *
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
Line.prototype.contains = function (mx, my) {
    if (mx < this.x0 || mx > this.x1) {
        return false;
    }


    function pointNearestMouse(line, x, y) {
        let lerp = function (a, b, x) {
            return (a + x * (b - a));
        };

        let dx = line.x1 - line.x0;
        let dy = line.y1 - line.y0;
        let t = ((x - line.x0) * dx + (y - line.y0) * dy) / (dx * dx + dy * dy);

        let lineX = lerp(line.x0, line.x1, t);
        let lineY = lerp(line.y0, line.y1, t);
        return ({x: lineX, y: lineY});
    }


    let linePoint = pointNearestMouse(this, mx, my);
    let dx = mx - linePoint.x;
    let dy = my - linePoint.y;
    let distance = Math.abs(Math.sqrt(dx * dx + dy * dy));

    // Save point for drawing selector
    this.linePointX = linePoint.x;
    this.linePointY = linePoint.y;

    let tolerance = 3;
    return distance < tolerance;
};

/**
 * Checks whether TextField object contains point where mouse is at.
 * @param mx x coordinate of a mouse
 * @param my y coordinate of a mouse
 * @returns {boolean} true if point is contained
 */
TextField.prototype.contains = function (mx, my) {
    return containsForRectangle(mx, my, this);
};


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Helper functions for setting up drawing, context and other options
   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/**
 * Helper function for check if mouse is contained in a rectangle.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @param o object to check
 * @returns {boolean} true if contains
 */
function containsForRectangle(mx, my, o) {
    return (o.x <= mx) && ((o.x + o.w) >= mx) &&
        (o.y <= my) && ((o.y + o.h) >= my);
}


function setPointOfSelection(ctx, ref) {
    // Draw point
    ctx.save();
    ctx.beginPath();
    ctx.arc(ref.linePointX, ref.linePointY, 5, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
}


/**
 * Set no selection options.
 *
 * @param ctx context to set selection to
 */
function setNoSelection(ctx) {
    ctx.shadowBlur = 0;
    ctx.shadowColor = "#000000";
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
}


/**
 * Sets default selection.
 *
 * @param ctx context to set selection to
 * @param shadowBlur amount of blur for shadow
 * @param shadowColor color of shadow
 * @param offX x offset from top left corner of shape
 * @param offY y offset from top left corner of shape
 */
function setDefaultSelection(ctx, shadowBlur, shadowColor, offX, offY) {
    ctx.shadowBlur = shadowBlur || 10;
    ctx.shadowColor = shadowColor || "#000000";
    ctx.shadowOffsetX = offX || 1;
    ctx.shadowOffsetY = offY || 1;
}


/**
 * Sets context to fill and/or stroke depending on current reference given.
 *
 * @param ref reference to look for options
 * @param ctx context to change
 */
function canvasStrokeAndFill(ref, ctx) {
    if (ref.fillShape) {
        ctx.fill();
    }
    if (ref.strokeShape) {
        ctx.stroke();
    }
}


/**
 * Sets up context from given shape object.
 *
 * @param ctx context to setup (defaults to itself if any attribute for undefined shape properties)
 * @param o shape object
 */
function setupContext(ctx, o) {
    // Color and shadow options
    ctx.fillStyle = o.fillStyle || ctx.fillStyle;
    ctx.strokeStyle = o.strokeStyle || ctx.strokeStyle;
    ctx.shadowColor = o.shadowColor || ctx.shadowColor;
    ctx.shadowBlur = o.shadowBlur || ctx.shadowBlur;
    ctx.shadowOffsetX = o.shadowOffsetX || ctx.shadowOffsetX;
    ctx.shadowOffsetY = o.shadowOffsetY || ctx.shadowOffsetY;

    if (o.transform !== undefined) {
        ctx.shadowOffsetX -= o.transform.translation.x;
        ctx.shadowOffsetY -= o.transform.translation.y;
    }

    // Line style
    ctx.lineCap = o.lineCap || ctx.lineCap;
    ctx.lineWidth = o.lineWidth || ctx.lineWidth;
    ctx.setLineDash(o.lineDash || []);

    // Font properties
    ctx.font = o.font || ctx.font;
    ctx.textAlign = o.textAlign || ctx.font;
    ctx.textBaseline = o.textBaseline || ctx.textBaseline;

}
