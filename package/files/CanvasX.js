/**
 * Canvas state represents an object which holds all information about canvas.
 * Does translations, scaling and drawing of object by itself. Non modificable version.
 *
 * @param canvas holds HTML canvas object
 * @param xOptions object which holds various properties for this canvas
 * @param constants constants used by canvas
 * @constructor
 */
function CanvasX(canvas, xOptions, constants) {
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');

    // Canvas properties
    this.width = canvas.width;
    this.height = canvas.height;

    // Background
    this.backgroundColor = constants.colors.canvas.backgroundColor || "#12CBCD";

    this.xOptions = xOptions;

    this.constants = constants;

    // Canvas mouse tracker
    this.enableMouseTracking = true;
    this.mouseX = 0;
    this.mouseY = 0;
    this.mouseLabel = "mx: " + this.mouseX + ",  my: " + this.mouseY;


    // Properties for tracking canvas state and canvas objects
    this.activeStateID = null;
    this.dragg = false;
    this.draggX = 0;
    this.draggY = 0;

    // Define later
    this.selectedShapes = [];
    this.hoveredShapes = [];

    this.valid = false;
    this.shapes = [];

    // Events properties
    this.registeredEvents = [];


    // Reference to CanvasX
    let myState = this;

    /* // Add Canvas interaction only if canvas is allowed to change.
     if (xOptions.readOnly === false) {
         this.addEventListeners(myState);
     } else {
         this.addStaticEventListeners(myState);
     }*/
}
