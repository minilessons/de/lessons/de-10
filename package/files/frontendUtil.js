/**
 * Converts radians to degrees.
 * @param r angle in radians
 * @returns {number} angle in degrees
 */
function radToDeg(r) {
    return r * 180 / Math.PI;
}


/**
 * Converts degrees to radians.
 * @param d degree
 * @returns {number} angle in radians
 */
function degToRad(d) {
    return d * Math.PI / 180;
}


/**
 * Gets mouse coordinates
 *
 * @param canvas canvas in which mouse is at
 * @param e event object
 * @returns object which holds x and y position of current mouse location
 */
function getMouse(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    return {x: parseInt(pos.x), y: parseInt(pos.y)}
}


/**
 * Transforms hex number (base 16) to decimal (base 10).
 *
 * @param hexNumber hex number to convert to decimal
 * @returns {Number} converted number
 */
function hexToDecimal(hexNumber) {
    return parseInt(hexNumber, 16);
}


/**
 * Function constructor for RGBA color values.
 *
 * @param red amount of red
 * @param green amount of green
 * @param blue amount of blue
 * @param alpha amount of transparency
 * @constructor
 */
function RGBA(red, green, blue, alpha) {
    this.red = red;
    this.green = green;
    this.blue = blue;
    this.alpha = alpha;
    this.cssString = function () {
        return "rgba(" + this.red + "," + this.green + "," + this.blue + "," + this.alpha + ")"
    }
}


/**
 * Converts hex color to rgba.
 *
 * @param hexColor color to convert
 * @param alpha alpha value for new rgba color
 * @returns {RGBA} rgba representation of hex color
 */
function convertHexToRGBA(hexColor, alpha) {
    let alphaValue = alpha || 1.0;
    let red = hexToDecimal(hexColor.slice(1, 3));
    let green = hexToDecimal(hexColor.slice(3, 5));
    let blue = hexToDecimal(hexColor.slice(5, 7));
    return new RGBA(red, green, blue, alphaValue);
}


function getButtonType(ev) {
    let isRightClick;
    let isMiddleClick;
    let e = ev || window.event;

    if ("which" in e) {
        isRightClick = e.which === 3;
        isMiddleClick = e.which === 2;
    } else if ("button" in e) {
        isRightClick = e.button === 2;
        isMiddleClick = e.button === 1;
    }

    if (isRightClick) {
        return "right";
    } else if (isMiddleClick) {
        return "middle";
    } else {
        return "left";
    }
}


function computeFactors(size) {
    let a = 1;
    let factors = [];
    for (let c = 1; c <= (size + 1); c++) {
        factors.push(a);
        a = a * (size - c + 1) / c;
    }
    return factors;
}