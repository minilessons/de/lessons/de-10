/**
 * Main prototypes for CanvasX.
 * Initializes CanvasX prototypes before their usage.
 */
function initPrototypes() {
    /**
     * Redraws whole canvas.
     */
    CanvasX.prototype.invalidate = function () {
        this.valid = false;
        this.draw();
    };

    /**
     * Sets canvas state to provided state.
     * @param state state to set
     */
    CanvasX.prototype.setCanvasState = function (state) {
        "use strict";
        this.shapes = state.shapes;
        this.invalidate();
    };

    /**
     * Canvas reset to original clear state.
     */
    CanvasX.prototype.resetCanvas = function () {
        this.draggX = 0;
        this.draggY = 0;
        this.dragg = false;

        this.shapes = [];
        this.invalidate();
    };

    /**
     * Exports canvas object.
     */
    CanvasX.prototype.exportState = function () {
        return JSON.stringify(this);
    };

    CanvasX.prototype.exportWithCondition = function (predicate) {
        let filtered = this.shapes.filter(predicate);
        return JSON.stringify(filtered);
    };


    // Adders for CanvasX

    CanvasX.prototype.registerEvent = function (event) {
        "use strict";

        // Check if event type is supported
        this.registeredEvents.push(event);

        // Should we invalidate?
    };


    CanvasX.prototype.getEventByID = function (ID) {
        return this.getElementByID(this.registeredEvents, ID);
    };

    CanvasX.prototype.removeRegisteredEvent = function (eventID) {
        let index = this.registeredEvents.indexOf(this.getEventByID(eventID));
        if (index > -1) {
            this.registeredEvents.splice(index, 1);

        } else {
            throw "Nothing found for event ID: " + eventID;
        }
    };


    /**
     * Adds a shape to canvas.
     *
     * @param shape specified shape
     */
    CanvasX.prototype.addShape = function (shape) {
        this.shapes.push(shape);
    };

    /**
     * Clears all canvas drawings on context which canvas represents.
     */
    CanvasX.prototype.clear = function () {
        this.ctx.save();
        this.ctx.clearRect(0, 0, this.width, this.height);
        this.ctx.fillStyle = convertHexToRGBA(this.backgroundColor, 0.0).cssString();
        this.ctx.fillRect(0, 0, this.width, this.height);
        this.ctx.restore();
    };

    /**
     * Clears shapes array.
     */
    CanvasX.prototype.removeShapes = function () {
        this.shapes = [];
        this.invalidate();
    };

    /**
     * Applies predicate to shape objects.
     *
     * @param shapeID object to apply to
     * @param predicate function to call for each shape with given ID
     */
    CanvasX.prototype.doOnShape = function (shapeID, predicate) {
        this.getShapesByID(shapeID).forEach(predicate);
    };

    /**
     * Checks if canvas contains point provided.
     *
     * @param mx x coordinate of a point
     * @param my y coordinate of a point
     * @returns {boolean} true if canvas contains point
     */
    CanvasX.prototype.contains = function (mx, my) {
        let rect = this.canvas.getBoundingClientRect();
        return ((mx < this.width) && (mx > rect.left) && (my > rect.top) && (my < rect.height));
    };


    /**
     * Draws all specified elements in this canvas. Only if they are not up to date.
     */
    CanvasX.prototype.draw = function () {
        if (!this.valid) {
            let ctx = this.ctx;
            let shapes = this.shapes;

            // Clear the canvas
            this.clear();


            // Draw the shapes again
            let l = shapes.length;
            for (let c = 0; c < l; c++) {
                let sh = shapes[c];
                if (false === sh.isVisible) {
                    continue;
                }


                ctx.save();
                ctx.beginPath();
                sh.draw(ctx);
                ctx.restore();

            }

            if (this.enableMouseTracking) {
                ctx.save();
                ctx.beginPath();
                ctx.fillStyle = "#23BCDA";
                ctx.rect(5, 5, 150, 50);
                ctx.fill();
                ctx.stroke();
                ctx.font = this.constants.fonts.normalFont;
                ctx.strokeText(this.mouseLabel, 8, 25, 200);
                ctx.restore();
            }


            this.valid = true;
        }
    };


    /**
     *  Gets a shape by ID (only one)
     *
     * @param ID id of a shape
     * @returns {shape} returns first found shape by specified ID
     */
    CanvasX.prototype.getShapeByID = function (ID) {
        return this.getShapesByID(ID)[0];
    };

    /**
     * Removes shapes by ID.
     *
     * @param ID id of all shapes to remove
     */
    CanvasX.prototype.removeShapesByID = function (ID) {
        let shapesToRemove = this.getShapesByID(ID);

        shapesToRemove.forEach((shape) => {
            this.removeShapeByID(shape.ID);
        });
    };

    CanvasX.prototype.removeElementByID = function (elements, ID) {
        let index = elements.indexOf(ID);
        if (index > -1) {
            elements.splice(index, 1);
        } else {
            throw "Nothing 111found for ID: " + ID;
        }
    };

    /**
     * Removes shape object by id
     *
     * @param ID id of an object to remove
     */
    CanvasX.prototype.removeShapeByID = function (ID) {
        let index = this.shapes.indexOf(this.getShapeByID(ID));
        if (index > -1) {
            this.shapes.splice(index, 1);
        } else {
            throw "Nothing found for ID: " + ID;
        }
    };

    /**
     * Gets shapes by group identification
     *
     * @param groupID group identification
     * @returns {Array} list of grouped shapes by given identification
     */
    CanvasX.prototype.getShapesByGroupID = function (groupID) {
        let shapes = this.shapes;
        let retStream = [];
        shapes.forEach((shape) => {
            if (shape.groupID === groupID) {
                retStream.push(shape)
            }
        });
        return retStream;
    };

    CanvasX.prototype.getElementByID = function (elements, ID) {
        return this.getElementsByID(elements, ID)[0];
    };

    CanvasX.prototype.getElementsByID = function (elements, ID) {
        let retStream = [];
        elements.forEach((element) => {
            if (element.ID !== undefined) {
                if (element.ID === ID) {
                    retStream.push(element);
                }
            }
        });

        if (retStream.length === 0) {
            throw ("No 555element for ID: " + ID);
        }
        return retStream;
    };

    /**
     * Gets shapes by identification
     *
     * @param ID identification
     * @returns {Array} list of all shapes which has specified ID
     */
    CanvasX.prototype.getShapesByID = function (ID) {
        return this.getElementsByID(this.shapes, ID);
    };

    /**
     * Adds canvas interaction but without options for changing state provided. Just mouse move and selector events.
     *
     * @param myState
     */
    CanvasX.prototype.addStaticEventListeners = function (myState) {
        this.canvas.addEventListener("selectstart", function (e) {
            e.preventDefault();
        }, false);
        this.canvas.addEventListener("contextmenu", function (e) {
            e.preventDefault();
            return false;
        }, false);

        this.canvas.addEventListener("mousemove", mouseMoveCallback, false);


        function mouseMoveCallback(e) {
            // Selections and so on and so on...
        }
    };

    /**
     * Adds canvas interaction.
     *
     * @param myState reference to canvasX
     */
    CanvasX.prototype.addEventListeners = function (myState) {
        // Event listeners
        this.canvas.addEventListener("selectstart", function (e) {
            e.preventDefault();
        }, false);
        this.canvas.addEventListener("contextmenu", function (e) {
            e.preventDefault();
            return false;
        }, false);


        // Mouse events
        this.canvas.addEventListener("mousedown", mouseDownCallback, true);
        this.canvas.addEventListener("mousemove", mouseMoveCallback, false);
        this.canvas.addEventListener("mouseup", mouseUpCallback, false);


        function commonEventJob(e, userEvent, eventType) {
            // Type might not be good for specific events;
            let mouse = getMouse(myState.canvas, e);
            let type = getButtonType(e);
            if (type !== "left" && type !== "right" && type !== "middle") {
                throw ("Wrong event Type::" + type);
            }

            if (userEvent.type === eventType) {
                let retVal;
                // Get shape and check if mouse is inside shape
                //console.log("Accessing event::" + userEvent.ID);
                let shape = myState.getShapeByID(userEvent.shapeID);
                let inside = shape.contains(mouse.x, mouse.y);

                if (true === userEvent.alwaysCall) {
                    // Callback for mousedown: event object, shape, mouse position x, y, type of event;
                    retVal = userEvent.callback(e, shape, mouse.x, mouse.y, type, inside, myState);
                } else if (true === inside) {
                    retVal = userEvent.callback(e, shape, mouse.x, mouse.y, type, inside, myState);
                } else {
                    // Don't call
                    console.log("No call for id: " + userEvent.shapeID, "user defined event Type: " + userEvent.type);
                }

                myState.invalidate();
                return retVal;
            }
        }


        function setCanvasHovering(e) {
            let mouse = getMouse(myState.canvas, e);
            let mx = mouse.x;
            let my = mouse.y;
            myState.shapes.forEach((sh) => {
                let inside = sh.contains(mx, my);
                if (typeof sh.setHovering === "function") {
                    sh.setHovering(inside);
                } else {
                    sh.hovered = inside;
                }

                if (inside) {
                    myState.hoveredShapes.push(sh);
                } else {
                    // Remove if exists
                    if (sh.hovered === true) {
                        myState.removeElementByID(myState.hoveredShapes, sh.ID);
                    }
                }
            });

            myState.invalidate();
        }


        function setCanvasSelectors(e) {
            let mouse = getMouse(myState.canvas, e);
            let mx = mouse.x;
            let my = mouse.y;

            // Cannot have more than one selections at a time. Until button for connecting is implemented.
            if (myState.selectedShapes.length >= 1) {
                //return;
            }
            myState.shapes.forEach((sh) => {
                if (!sh.ID.startsWith("State")) {
                    return;
                }
                if (sh.contains(mx, my)) {
                    let type = getButtonType(e);
                    if (type === "left") {
                        if (typeof sh.setSelection === "function") {
                            sh.setSelection(true);
                        } else {
                            sh.selected = true;
                        }

                        if (!myState.selectedShapes.includes(sh.ID)) {
                            console.log("Adding selection for ID::", sh.ID);
                            myState.selectedShapes.push(sh.ID);
                        }
                    } else if (type === "right") {
                        if (typeof sh.setSelection === "function") {
                            sh.setSelection(false);
                        } else {
                            sh.selected = false;
                        }

                        // Remove if exists
                        if (myState.selectedShapes.includes(sh.ID)) {
                            myState.removeElementByID(myState.selectedShapes, sh.ID);
                        }
                    }
                }
            });

            myState.invalidate();
        }


        function mouseDownCallback(e) {
            setCanvasSelectors(e);
            console.log("Got to mouse down");

            // Go through registered events and call provided callback methods
            let callGlobal = true;
            let first = true;
            myState.registeredEvents.forEach((event) => {
                let retVal = commonEventJob(e, event, "mousedown");
                if (retVal && first) {
                    callGlobal = false;
                    first = false;
                }
            });


            if (typeof globalMouseDown === "function" && callGlobal) {
                globalMouseDown(e, myState);
            }

        }


        function mouseMoveCallback(e) {
            setCanvasHovering(e);
            // Serve global mouse move event
            if (typeof globalMouseMove === "function") {
                globalMouseMove(e, myState);
            }

            // Set mouse tracking elements:
            if (myState.enableMouseTracking) {
                let mouse = getMouse(myState.canvas, e);
                myState.mouseX = Math.round(mouse.x);
                myState.mouseY = Math.round(mouse.y);
                myState.mouseLabel = "mx: " + myState.mouseX + ",  my: " + myState.mouseY;

                myState.invalidate();

            }


            // Go through registered events and call provided callback methods
            myState.registeredEvents.forEach((event) => {
                commonEventJob(e, event, "mousemove");
            });
        }


        function mouseUpCallback(e) {
            // Serve global mouse move event
            if (typeof globalMouseUp === "function") {
                globalMouseUp(e, myState);
            }

            // Go through registered events and call provided callback methods
            myState.registeredEvents.forEach((event) => {
                commonEventJob(e, event, "mouseup",);
            });
        }


        // Keyboard events
        this.canvas.addEventListener("keydown", keyDownCallback, false);
        this.canvas.addEventListener("keypress", keyPressCallback, false);


        function keyDownCallback(e) {
            // Go through registered events and call provided callback methods
            myState.registeredEvents.forEach((event) => {
                commonEventJob(e, event, "keydown");
            });
        }


        function keyPressCallback(e) {
            // Go through registered events and call provided callback methods
            myState.registeredEvents.forEach((event) => {
                commonEventJob(e, event, "keypress");
            });
        }
    }
}


initPrototypes();
