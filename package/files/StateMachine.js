const RENDER_STATES = {
    PASSIVE_STATE: 0,
    ACTIVE_STATE: 1,
    START_STATE: 2,
    BEZIER_CURVE: 3,
    SELECTED_OBJECT: 4
};


function StateMachine(alphabet) {
    this.alphabet = alphabet;

    this.states = [];
    this.transitions = [];

    this.simulate = function (inputSeries) {
        alert("Cannot simulate. Please implement simulation.");
    };


    this.removeStates = function (listOfStates) {
        let size = listOfStates.length;
        for (let c = 0; c < size; c++) {
            let idx = this.states.indexOf(listOfStates[c]);
            if (idx > -1) {
                this.states.splice(idx, 1);
            } else {
                throw new Error("Cannot remove state for list: " + listOfStates);
            }
        }


    };
}


function MachineState(x, y, output, renderState) {
    this.startState = false;
    this.position = {x: x, y: y};
    this.output = output;
    this.renderState = renderState || RENDER_STATES.PASSIVE_STATE;
}


function MachineTransition(first, second, condition, renderState) {
    this.first = first;
    this.second = second;
    this.condition = condition;
    this.renderState = renderState || RENDER_STATES.BEZIER_CURVE;
}