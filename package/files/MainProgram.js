const PROGRAM_STATES = {
    DEFAULT_STATE: 0,
    ADDING_STATE: 1,
    REMOVING_STATE: 2,
    ADDING_TRANSITION: 3
};


function CurrentState(model, re) {
    this.model = model;
    this.state = PROGRAM_STATES.DEFAULT_STATE;
    this.currentObject = null;
    this.selections = [];
    const renderer = re;

    this.paint = function () {
        if (this.currentObject !== null) {
            renderer.renderUnknown(this.currentObject);
        }
    };


    this.checkSelections = function (x, y) {
        renderer.activeAreas.forEach((area) => {
            if (area.primitive.contains(x, y)) {
                if (area.objReference.renderState !== RENDER_STATES.SELECTED_OBJECT) {
                    area.objReference.renderState = RENDER_STATES.SELECTED_OBJECT;
                    this.selections.push(area.objReference);
                } else {
                    area.objReference.renderState = RENDER_STATES.PASSIVE_STATE;
                }
            }
        });
    };

    this.mouseMoved = function (x, y) {
        switch (this.state) {
            case PROGRAM_STATES.ADDING_STATE:
                this.currentObject.position.x = x;
                this.currentObject.position.y = y;
                return true;
                break;
            default:
                return false;
        }
    };

    this.mouseClicked = function (x, y) {
        switch (this.state) {
            case PROGRAM_STATES.ADDING_STATE:
                this.addState(this.currentObject);
                this.currentObject = null;
                this.state = PROGRAM_STATES.DEFAULT_STATE;
                break;
            case PROGRAM_STATES.ADDING_TRANSITION:
                if (this.selections.length !== 2) {
                    this.checkSelections(x, y);

                    if (this.selections.length === 2) {
                        this.addTransition();
                        this.selections = [];
                        this.state = PROGRAM_STATES.DEFAULT_STATE;
                    }
                    break;
                }
                break;
            case PROGRAM_STATES.REMOVING_STATE:
                this.model.removeStates(this.selections);
                this.state = PROGRAM_STATES.DEFAULT_STATE;
                break;
            default:
                // Default state, check selections
                this.checkSelections(x, y);
        }
    };

    this.addState = function (state) {
        this.model.states.push(state);
    };

    this.addTransition = function () {
        let f = this.selections[0];
        let s = this.selections[1];
        this.model.transitions.push(new MachineTransition(f, s, "0"));
    };
}


function ActiveArea(refToObj, primitive) {
    this.primitive = primitive;
    this.objReference = refToObj;

}


function MachineRenderer(canvasX) {
    this.canvas = canvasX.canvas;
    this.ctx = canvasX.ctx;
    this.myContext = {
        PASSIVE_COLOR: "blue",
        ACTIVE_COLOR: "red"
    };
    this.activeAreas = [];

    // Render info
    this.radius = 12 * Math.PI;

    this.renderUnknown = function (obj) {
        if (obj instanceof MachineState) {
            this.renderState(obj);
        } else if (obj instanceof MachineTransition) {
            this.renderTransition(obj);
        } else {
            throw new Error("Rendering Error: 'Cannot render object' for object: " + obj);
        }
    };

    this.render = function (model) {
        this.activeAreas = [];
        let numOfStates = model.states.length;
        for (let c = 0; c < numOfStates; c++) {
            this.renderState(model.states[c]);
        }

        let numOfTransitions = model.transitions.length;
        for (let c = 0; c < numOfTransitions; c++) {
            this.renderTransition(model.transitions[c]);
        }

    };

    this.setupStateContext = function (state, ctx) {
        switch (state.renderState) {
            case RENDER_STATES.PASSIVE_STATE:
                ctx.fillStyle = this.myContext.PASSIVE_COLOR;
                ctx.strokeStyle = this.myContext.PASSIVE_COLOR;
                break;
            case RENDER_STATES.ACTIVE_STATE:
                ctx.fillStyle = this.myContext.ACTIVE_COLOR;
                ctx.strokeStyle = this.myContext.ACTIVE_COLOR;
                break;
            case RENDER_STATES.SELECTED_OBJECT:
                ctx.fillStyle = this.myContext.ACTIVE_COLOR;
                ctx.strokeStyle = this.myContext.ACTIVE_COLOR;
                break;
        }
    };

    this.renderState = function (state) {
        this.ctx.save();
        this.setupStateContext(state, this.ctx);

        let circleShape = this.constructCircleFromState(state);
        circleShape.drawShape(this.ctx);
        this.ctx.restore();

        this.ctx.strokeText(state.output, state.position.x, state.position.y + 15);

        this.activeAreas.push(new ActiveArea(state, circleShape));
    };

    this.constructCircleFromState = function (state) {
        return new Circle("tempCircleID", state.position.x, state.position.y, this.radius, {fillStyle: false});
    };

    this.renderTransition = function (t) {
        let state1 = {circle: this.constructCircleFromState(t.first)};
        let state2 = {circle: this.constructCircleFromState(t.second)};

        let vertices = constructVertices(state1, state2);
        let curve = new BezierCurve(vertices);

        curve.drawBezier(this.ctx);
        curve.drawControlPoints(this.ctx);
    };
}


function MainProgram(c) {
    const model = new StateMachine(["0", "1"]);
    const canvasX = c;
    const machineRenderer = new MachineRenderer(canvasX);
    const currentState = new CurrentState(model, machineRenderer);
    const eventOpts = {
        drag: false,
        lastX: 0,
        lastY: 0,
        restoreObject: null,
        selectedArea: null,
        selectionIdentification: -1

    };

    const restrictRect = new Rectangle("restrictRect", 0, 100, canvasX.canvas.width, canvasX.canvas.height, convertHexToRGBA("#FFFFFF", 0.0), {});
    const stateMachineComponent = new DrawComponent(restrictRect);


    function paint() {
        canvasX.invalidate();
        machineRenderer.render(model);
        currentState.paint(machineRenderer);
    }


    function addListeners() {
        canvasX.canvas.addEventListener("mousedown", mouseDownEvent.bind(null, canvasX), false);
        canvasX.canvas.addEventListener("mousemove", mouseMoveEvent.bind(null, canvasX), false);
        canvasX.canvas.addEventListener("mouseup", mouseUpEvent.bind(null, canvasX), false);
    }


    addListeners();


    function mouseMoveEvent(canX, e) {
        let mouse = getMouse(canX.canvas, e);
        let doPaint = currentState.mouseMoved(mouse.x, mouse.y);


        if (!eventOpts.drag || (eventOpts.selectedArea === null)) {
            if (doPaint) {
                paint();
            }
            return;
        }

        // Something is dragging
        let area = eventOpts.selectedArea;
        area.objReference.position.x = mouse.x;
        area.primitive.x = mouse.x;
        area.objReference.position.y = mouse.y;
        area.primitive.y = mouse.y;

        eventOpts.lastX = mouse.x;
        eventOpts.lastY = mouse.y;

        paint();
    }


    function checkStateAcceptance() {
        function getDistance(state, other) {
            let c = state.circle;
            return Math.sqrt((Math.pow(c.x - other.circle.x, 2) + Math.pow(c.y - other.circle.y, 2)));
        }


        let testState = {circle: eventOpts.selectedArea.primitive};
        let isSpotAvailable = true;
        let numOfActives = machineRenderer.activeAreas.length;
        for (let c = 0; c < numOfActives; c++) {
            const area = machineRenderer.activeAreas[c];
            let otherState = {circle: area.primitive};
            let radius = otherState.circle.radius;

            if (eventOpts.selectionIdentification !== c) {
                let distanceFromCenters = getDistance(otherState, testState);
                if (distanceFromCenters < (2 * radius)) {
                    isSpotAvailable = false;
                }
            }
        }

        return isSpotAvailable;
    }


    function mouseUpEvent(canX, e) {
        if (!eventOpts.drag || (eventOpts.selectedArea === null)) {
            return;
        }

        eventOpts.drag = false;
        //eventOpts.selectedArea.objReference.renderState = RENDER_STATES.PASSIVE_STATE;

        if (!checkStateAcceptance()) {
            // Restore selected shape
            let area = eventOpts.selectedArea;
            area.objReference.position.x = eventOpts.restoreObject.centerX;
            area.objReference.position.y = eventOpts.restoreObject.centerY;
        }

        eventOpts.selectedArea = null;
        eventOpts.restoreObject = null;

        paint();
    }


    function mouseDownEvent(canX, e) {
        let m = getMouse(canX.canvas, e);
        let shapes = canX.shapes;

        let size = shapes.length;
        for (let c = 0; c < size; c++) {
            if (shapes[c].contains(m.x, m.y)) {
                dispatchMouseClick(shapes[c].ID, canX);
                break;
            }
        }

        if (stateMachineComponent.contains(m.x, m.y)) {
            currentState.mouseClicked(m.x, m.y);

            let areas = machineRenderer.activeAreas;
            let numOfActives = areas.length;
            for (let c = 0; c < numOfActives; c++) {
                if (areas[c].primitive.contains(m.x, m.y)) {
                    eventOpts.drag = true;
                    eventOpts.lastX = m.x;
                    eventOpts.lastY = m.y;
                    eventOpts.restoreObject = {
                        centerX: areas[c].primitive.x,
                        centerY: areas[c].primitive.y
                    };
                    eventOpts.selectedArea = areas[c];
                    eventOpts.selectionIdentification = c;
                    break;
                }
            }
        }

        paint();
    }


    function dispatchMouseClick(shapeID, c) {
        // Which button is pressed
        switch (shapeID) {
            case c.constants.strings.addStateBtnID:
                currentState.currentObject = new MachineState(0, 0, "00");
                currentState.state = PROGRAM_STATES.ADDING_STATE;
                break;
            case c.constants.strings.addTransitionBtnID:
                currentState.state = PROGRAM_STATES.ADDING_TRANSITION;
                break;
            case c.constants.strings.exportBtnID:
                // Export to text area
                //exportStateToArea(canvas);
                break;
            case c.constants.strings.importBtnID:
                //importStateFromArea(canvas);
                break;
            case c.constants.strings.removeStateBtnID:
                break;

            default:
                throw new Error("Cannot dispatch mouse click on ID: " + shapeID);
        }
    }
}
