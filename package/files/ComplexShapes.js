function DrawComponent(Rectangle) {
    this.rectangle = Rectangle;
}


DrawComponent.prototype.contains = function (mx, my) {
    return containsForRectangle(mx, my, this.rectangle);
};


function Transition(id, curve, state1, state2) {
    this.ID = id;
    this.curve = curve;
    this.state1Id = state1;
    this.state2Id = state2;
}


Transition.prototype.contains = function (mx, my) {
    //console.log("clicked on transition");
    return false;
};

Transition.prototype.draw = function (ctx) {
    this.curve.drawControlPoints(ctx);
    this.curve.draw(ctx);
};

BezierCurve.prototype.drawControlPoints = function (ctx) {
    ctx.save();

    ctx.beginPath();
    ctx.fillStyle = "#FF0000";
    this.vertices.forEach((v) => {
        ctx.fillRect(v.x, v.y, 2, 2);
    });
    ctx.restore();
};


function constructVertices(state1, state2) {
    let circle1 = state1.circle;
    let circle2 = state2.circle;

    let startY = circle1.y - circle1.radius / 2;
    let startX = circle1.x;

    let distance = Math.abs(circle1.x - circle2.x);
    let centerX = 0;
    let centerY = 0;

    let endY = circle2.y - circle2.radius / 2;
    let endX = circle2.x;

    let startTmp = Math.sqrt(Math.pow(circle1.radius, 2) - Math.pow(circle1.radius / 2, 2));
    let endTmp = Math.sqrt(Math.pow(circle2.radius, 2) - Math.pow(circle2.radius / 2, 2));

    let v = [startX, startY, centerX, centerY, endX, endY];
    if (v[0] < v[4]) {
        v[0] = v[0] + startTmp;
        v[4] = v[4] - endTmp;
        centerX = circle1.x + distance / 2;
        centerY = circle2.y - 2 * circle2.radius;
    } else {
        v[0] = v[0] - startTmp;
        v[4] = v[4] + endTmp;
        centerX = circle2.x + distance / 2;
        centerY = circle2.y - 2 * circle2.radius;
    }


    v[2] = centerX;
    v[3] = centerY;

    v = v.map((v) => {
        return Math.round(v);
    });

    let size = v.length;
    let vertices = [];
    for (let c = 0; c < size; c += 2) {
        let newVector = {x: v[c], y: v[c + 1]};
        vertices.push(newVector);
    }

    return vertices;
}


/**
 * Simple representation of automateState for constructing state machines.
 *
 * @param ID state identification
 * @param Circle object used to draw this state
 * @param TextArea text object used to add text inside state
 * @constructor
 */
function AutomateState(ID, Circle, TextArea) {
    this.ID = ID;

    this.circle = Circle;
    this.textArea = TextArea;

    this.transitions = [];

    this.setSelection = function (selected) {
        this.circle.selected = selected;
    };

    this.setHovering = function (hovered) {
        this.circle.hovered = hovered;
    };

    this.addTransition = function (id) {
        this.transitions.push(id);
    };

    // Visibility
    this.isVisible = true;
}


AutomateState.prototype.contains = function (mx, my) {
    return this.circle.contains(mx, my);
};


AutomateState.prototype.updateTransitions = function (canvas) {
    this.transitions.forEach((transitionId) => {
        let t = canvas.getShapeByID(transitionId);
        t.updateData(canvas);
    });
};

AutomateState.prototype.draw = function (ctx) {
    // Draw Circle first, because of possible overlay and fill options
    this.circle.draw(ctx);

    // Draw text inside Circle next
    this.textArea.draw(ctx);

};


function BezierCurve(vertices, divs) {
    this.vertices = vertices;
    this.divs = divs || 100;

    this.bezierVertices = this.constructPoints(this.divs);
}


Transition.prototype.removeTransition = function (canvas) {
    let state1 = canvas.getShapeByID(this.state1Id);
    let state2 = canvas.getShapeByID(this.state2Id);

    let checkFirst = state1.transitions.indexOf(this.ID);
    let checkSecond = state2.transitions.indexOf(this.ID);
    if (checkFirst > -1) {
        state1.transitions.splice(checkFirst, 1);
    }

    if (checkSecond > -1) {
        state2.transitions.splice(checkSecond, 1);
    }

};

Transition.prototype.updateData = function (canvas) {
    let state1 = canvas.getShapeByID(this.state1Id);
    let state2 = canvas.getShapeByID(this.state2Id);
    let curve = this.curve;
    curve.vertices = constructVertices(state1, state2);
    curve.bezierVertices = curve.constructPoints(curve.divs);
};


BezierCurve.prototype.constructPoints = function (divs) {
    let numOfPoints = (this.vertices.length - 1);
    let factors = computeFactors(numOfPoints);
    let bezierVertices = [];
    let t, b;
    let current = [0, 0];

    for (let c = 0; c <= divs; c++) {
        t = 1.0 / divs * c;
        current[0] = 0;
        current[1] = 0;
        for (let k = 0; k <= numOfPoints; k++) {
            if (k === 0) {
                b = factors[k] * Math.pow(1 - t, numOfPoints);
            } else if (k === numOfPoints) {
                b = factors[k] * Math.pow(t, numOfPoints);
            } else {
                b = factors[k] * Math.pow(t, k) * Math.pow(1 - t, numOfPoints - k);
            }
            current[0] += (b * this.vertices[k].x);
            current[1] += (b * this.vertices[k].y);
        }

        let newVector = {x: current[0], y: current[1]};
        bezierVertices.push(newVector);
    }

    return bezierVertices;
};


/**
 * Function constructor for MyButton shape.
 *
 * @param ID identification
 * @param x x coordinate of top left rectangle point
 * @param y y coordinate of top left rectangle point
 * @param w width of a shape
 * @param h height of a shape
 * @param text text inside shape
 * @param opts other options
 * @constructor
 */
function MyButton(ID, x, y, w, h, text, opts) {
    this.ID = ID || undefined;
    this.groupID = undefined;

    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;

    // Button styles
    this.fillStyle = opts.fillStyle || "#0AAAAA";
    this.strokeStyle = opts.strokeStyle || "#000000";

    this.alphaNonTransparent = opts.alphaNonTransparent || 1;
    this.alphaTransparent = opts.alphaTransparent || 0.5;

    this.font = opts.font;
    this.textColor = opts.textColor || "#000000";
    this.textBaseline = opts.textBaseline || "middle";
    this.textMaxSpan = opts.textMaxSpan || 50;
    this.textHeight = Number((opts.font.split("px")[0])) || 12;
    this.text = text || "";

    this.shadowBlur = opts.shadowBlur || 0;
    this.shadowColor = opts.shadowColor || "#000000";
    this.shadowOffsetX = opts.shadowOffsetX || 0;
    this.shadowOffsetY = opts.shadowOffsetY || 0;

    // Stroke and fill opts
    this.fillShape = (opts.fillShape === undefined) ? true : opts.fillShape;
    this.strokeShape = (opts.strokeShape === undefined) ? true : opts.strokeShape;

    // Frame selections
    this.frameSelectionColor = opts.frameSelectionColor || "#00AAAA";

    // Curved rectangle buttons
    this.curved = opts.curved || false;
    this.leftCurve = opts.leftCurve || 12;
    this.rightCurve = opts.rightCurve || 12;

    // Save opts as structure
    this.opts = opts;

    // Selection options
    this.selected = false;
    this.hovered = false;
    this.isPressed = false;

    // Visibility
    this.isVisible = true;
}


BezierCurve.prototype.draw = function (ctx) {
    let size = this.bezierVertices.length;

    ctx.save();
    ctx.beginPath();
    for (let c = 0; c < size; c++) {
        ctx.lineTo(this.bezierVertices[c].x, this.bezierVertices[c].y);
    }
    ctx.stroke();
    ctx.restore();


};

BezierCurve.prototype.drawQuadratic = function (ctx) {
    let v = this.vertices;
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "#00FF00";
    ctx.moveTo(v[0].x, v[0].y);
    ctx.quadraticCurveTo(v[1].x, v[1].y, v[2].x, v[2].y);
    ctx.stroke();
    ctx.restore();
};

BezierCurve.prototype.drawBezier = function (ctx) {
    let v = this.vertices;
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "#FF0000";
    ctx.moveTo(v[0].x, v[0].y);
    ctx.bezierCurveTo(v[1].x - v[1].x / 2, v[1].y, v[1].x + v[1].x / 2, v[1].y, v[2].x, v[2].y);
    ctx.stroke();
    ctx.restore();
};


/**
 * Prototype which is used to draw MyButton object.
 *
 * @param ctx context to draw to
 */
MyButton.prototype.draw = function (ctx) {
    ctx.save();
    setupContext(ctx, this);
    if (this.isPressed === true) {
        setNoSelection(ctx);
        this.drawShape(ctx);
    } else if (this.hovered === true) {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaNonTransparent).cssString();
        this.drawShape(ctx);
        ctx.restore();
    } else {
        ctx.save();
        ctx.fillStyle = convertHexToRGBA(this.fillStyle, this.alphaTransparent).cssString();
        setNoSelection(ctx);
        this.drawShape(ctx);
        ctx.restore();
    }


    // Draw text
    setNoSelection(ctx);
    this.drawText(ctx);
    ctx.restore();
};


/**
 * Actual drawing of MyButton shape.
 *
 * @param ctx context to draw to
 */
MyButton.prototype.drawShape = function (ctx) {
    if (this.curved === false) {
        ctx.beginPath();
        ctx.rect(this.x, this.y, this.w, this.h);
        canvasStrokeAndFill(this, ctx);
    } else {
        // Add rounded path and fill it...
        ctx.beginPath();
        let r = {
            left: this.leftCurve,
            right: this.rightCurve
        };

        ctx.moveTo(this.x + r.left, this.y);
        ctx.lineTo(this.x + this.w - r.right, this.y);
        ctx.quadraticCurveTo(this.x + this.w, this.y, this.x + this.w, this.y + r.right);
        ctx.lineTo(this.x + this.w, this.y + this.h - r.right);
        ctx.quadraticCurveTo(this.x + this.w, this.y + this.h, this.x + this.w - r.right, this.y + this.h);
        ctx.lineTo(this.x + r.left, this.y + this.h);
        ctx.quadraticCurveTo(this.x, this.y + this.h, this.x, this.y + this.h - r.left);
        ctx.lineTo(this.x, this.y + r.left);
        ctx.quadraticCurveTo(this.x, this.y, this.x + r.left, this.y);

        canvasStrokeAndFill(this, ctx);
    }
};


/**
 * Actual drawing of text for MyButton shape.
 *
 * @param ctx context to draw to
 */
MyButton.prototype.drawText = function (ctx) {
    // Set different color for text
    ctx.fillStyle = this.textColor;

    // MultiLineText
    let lines = this.text.split("\n");
    let numOfLines = lines.length;
    let offsetBetweenLines = 3;
    let lineHeight = this.textHeight + offsetBetweenLines;
    ctx.textMaxSpan = 80;

    let centerY = (this.h - numOfLines * this.textHeight) / 2;
    for (let c = 0; c < numOfLines; c++) {
        let textWidth = ctx.measureText(lines[c]).width;
        let numOfLetters = lines[c].length;
        let oneLetterUnit = textWidth / numOfLetters;

        let seenLetters = numOfLetters;
        let minOffset = 3;
        let widthOffset = 5;
        for (let c = 1; c <= numOfLetters; c++) {
            if (this.w < (minOffset + widthOffset + oneLetterUnit * c)) {
                seenLetters = c - 1;
                break;
            }
        }

        let centerX = (this.w - textWidth) / 2;

        let xText = Math.max(this.x + minOffset, minOffset + this.x + centerX);

        let textString = lines[c].slice(0, seenLetters);
        ctx.fillText(textString, xText, this.y + centerY + lineHeight * c + this.textHeight / 2, this.textMaxSpan);
    }

    ctx.stroke();
};


/**
 * Checks whether mouse is contained inside MyButton shape.
 *
 * @param mx x mouse coordinate
 * @param my y mouse coordinate
 * @returns {boolean} true if contains
 */
MyButton.prototype.contains = function (mx, my) {
    return containsForRectangle(mx, my, this);
};
