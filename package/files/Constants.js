/**
 * Main constants used for this project.
 * This constructor holds default values for all constants.
 *
 * Several constants object are provided: "colors", "fonts", "strings", "numeric" and "keyCodes". They offer various elements including a lot of colors, fonts, string constants, numeric constants and common key codes for keyboard presses.
 */
class Constants {
    constructor(colors = {
                    black: "black",
                    yellow: "yellow",
                    red: "red",
                    white: "white",
                    orange: "orange",
                    awesome: "#8836ba",
                    gray: "#AF1E37",
                    darkRed: "#d02b2a",
                    purple: "#8e44ad",
                    emerald: "#2ecc71",
                    midnight: "#2c3e50",
                    dodgerBlue: "#1E90FF",
                    flipD: "#1E90FF",
                    flipT: "#886F98",
                    diagramArrow: "#da0000",
                    flipSR: "#CB3A34",
                    flipJK: "#2ecc71",
                    registerColor: "#50928c",
                    clickBoxColor: "#746dc5",
                    darkBlue: "#00008B",
                    lightSkyBlue: "#87CEFA",
                    glowColor: "rgba(255, 0, 0, 0.5)",
                    fullTransparent: "rgba(255, 255, 255, 0)",
                    defaultPixel: "#b5e2cb",
                    selectedPixel: "#b65bab",
                    correctColor: "#71bf9a",
                    successBox: "#8cdf7b",
                    infoNiceBlue: "#2196F3",
                    pixelActiveInput: "#df120e",
                    startPoints: "#e23e41",
                    selectorColor: "#12ACBD",
                    gridColor: "#dda8d7",
                    letterF: "#123DBC"
                },
                fonts = {
                    pixelFont: "14px bold Schoolbook",
                    normalFont: "14px Schoolbook",
                    arialFont: "12px Arial",
                    startEndPoints: "24px Schoolbook",
                    axesFont: "12px Schoolbook"
                },
                strings = {
                    EMPTY: "",

                },
                numeric = {},
                keyCodes = {
                    left: 37,
                    up: 38,
                    right: 39,
                    down: 40,
                    enter: 13,
                    E: 69
                },
                smallLetters = {
                    /* Various text subscripts and superscripts */
                    /* Subscript <sub> x </sub> numbers */
                    sup0: '\u2070',
                    sup1: '\u00B9',
                    sup2: '\u00B2',
                    sup3: '\u00B3',
                    sup4: '\u2074',
                    sup5: '\u2075',
                    sup6: '\u2076',
                    sup7: '\u2077',
                    sup8: '\u2078',
                    sup9: '\u2079',

                    /* Superscript <sup> x </sup> numbers */
                    sub0: '\u2080',
                    sub1: '\u2081',
                    sub2: '\u2082',
                    sub3: '\u2083',
                    sub4: '\u2084',
                    sub5: '\u2085',
                    sub6: '\u2086',
                    sub7: '\u2087',
                    sub8: '\u2088',
                    sub9: '\u2089',

                    // Small sub letters
                    subA: '\u2090',
                    subE: '\u2091',
                    subO: '\u2092',
                    subM: '\u2098',
                    subN: '\u2099',

                    supN: '\u207F',

                    // Other signs
                    subPlus: '\u208A',
                    subMinus: '\u208B',
                    subBracketLeft: '\u208F',
                    supBracketLeft: '\u207D',
                    subBracketRight: '\u208E',
                    supBracketRight: '\u207E',
                }) {
        this.colors = colors;
        this.fonts = fonts;
        this.strings = strings;
        this.numeric = numeric;
        this.keyCodes = keyCodes;
        this.smallLetters = smallLetters;


    }
}


/**
 * Gets current project constants using additional specified constants in specs as well as other constants.
 *
 * @returns {Constants} Constants with specified constants
 */
function getLessonConstants() {

    let colors = {
        black: "black",
        yellow: "yellow",
        red: "red",
        white: "white",
        orange: "orange",
        awesome: "#8836ba",
        gray: "#AF1E37",
        darkRed: "#d02b2a",
        purple: "#8e44ad",
        emerald: "#2ecc71",
        midnight: "#2c3e50",
        dodgerBlue: "#1E90FF",
        darkBlue: "#00008B",
        lightSkyBlue: "#87CEFA",
        glowColor: "rgba(255, 0, 0, 0.5)",

        canvas: {
            backgroundColor: "#f3f3f3"
        },


        btns: {
            lightTeal: "#54c8bf",
            blueButton: "#2ac0ff",
            redButton: "#FF020C",
            greenButton: "#4ABC12",
            textBlack: "#00222A",
            whiteButton: "#ecedff",
            transparent: 0.7,
            nonTransparent: 1
        },

        selectionColors: {
            frameSelectionColor: "#94d0e7",
            frameStandardColor: "#0AAAAA",
        }


    };

    let fonts = {
        smallFont: "12px Schoolbook",
        //normalFont: "12px Schoolbook",
        normalFont: "14px Schoolbook",
        middleFont: "17px Schoolbook",
        arialFont: "12px Arial",
        axesFont: "12px Schoolbook",
        schoolBook: "14px Schoolbook",
        biggerFont: "20px Schoolbook"
    };

    let strings = {
        EMPTY: "",
        addStateBtnID: "addStateBtn",
        addID: "addStateBtn",
        exportBtnID: "exportStateBtn",
        importBtnID: "importStateBtn",
        addTransitionBtnID: "addTransitionBtn",
        removeStateBtnID: "removeStatesBtn"
    };

    let numeric = {};

    let keyCodes = {
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        enter: 13,
        E: 69,

    };
    let smallLetters = {
        /* Various text subscripts and superscripts */
        /* Subscript <sub> x </sub> numbers */
        sup0: '\u2070',
        sup1: '\u00B9',
        sup2: '\u00B2',
        sup3: '\u00B3',
        sup4: '\u2074',
        sup5: '\u2075',
        sup6: '\u2076',
        sup7: '\u2077',
        sup8: '\u2078',
        sup9: '\u2079',

        /* Superscript <sup> x </sup> numbers */
        sub0: '\u2080',
        sub1: '\u2081',
        sub2: '\u2082',
        sub3: '\u2083',
        sub4: '\u2084',
        sub5: '\u2085',
        sub6: '\u2086',
        sub7: '\u2087',
        sub8: '\u2088',
        sub9: '\u2089',

        // Small sub letters
        subA: '\u2090',
        subE: '\u2091',
        subO: '\u2092',
        subM: '\u2098',
        subN: '\u2099',

        supN: '\u207F',

        // Other signs
        subPlus: '\u208A',
        subMinus: '\u208B',
        subBracketLeft: '\u208F',
        supBracketLeft: '\u207D',
        subBracketRight: '\u208E',
        supBracketRight: '\u207E',
    };


    return new Constants(colors, fonts, strings, numeric, keyCodes, smallLetters);
}
